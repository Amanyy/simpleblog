@extends('layouts.layout')
@section('content')

<div class="container">
    <div class="row">
        <div class="postAdd col-lg-12 text-right" style="margin-bottom: 20px;">
            <a href="{{route('admin.blog-dashboard')}}" class="btn btn-success">Blog Dashboard</a>
        </div>  
    </div>
    @if (Session::has('message'))
    <div class="col-md-12">
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    </div>
    @endif
    <div class="container">
<div class="row">
        <div class="col-md-12">
            <h1>Edit Your Article Here</h1>
            {{ Form::model($article, array('route' => array('admin.post-edit-article', $article->id), 'method' => 'post')) }}

            <div class="form-group">

                @if(!empty($errors->first('title')))
                <div class="alert alert-danger">{{ $errors->first('title') }}</div>

                @endif
                {{ Form::label('title', 'Post Title') }}
                {{ Form::text('title', $value = null, array('class' => 'form-control', 'placeholder' => 'Your Blog Post Title')) }}
            </div>
            <div class="form-group">
                @if(!empty($errors->first('author_email')))
                <div class="alert alert-danger">{{ $errors->first('author_email') }}</div>
                @endif
                {{ Form::label('Auther', 'Auther Email') }}
                {{ Form::text('author_email', $value = null, array('class' => 'form-control', 'placeholder' => 'Auther Email')) }}
            </div>
            <div class="form-group">

                @if(!empty($errors->first('category_id')))
                <div class="alert alert-danger">{{ $errors->first('category_id') }}</div>
                @endif
                {{ Form::label('Category', 'Category') }}
                {{ Form::select('category_id', $categories, $value, array('class'=>'form-control')) }}

            </div>
            <div class="form-group">

                @if(!empty($errors->first('content')))
                <div class="alert alert-danger">{{ $errors->first('content') }}</div>

                @endif
                {{ Form::label('content', 'Blog Body/Content') }}
                {{ Form::textarea('content', $value = null, array('class' => 'form-control', 'rows' => '7')) }}

            </div>
            {{ Form::submit('Edit Your Blog Article', array('class' => 'btn btn-default')) }}
            {{ Form::close() }}
        </div>
    </div></div>
    @endsection
