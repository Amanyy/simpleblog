@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="postAdd col-lg-12 text-right" style="margin-bottom: 20px;">
            <a href="{{route('admin.create-article')}}" class="btn btn-success">Add Blog</a>
        </div>  
    </div>

    @if (Session::has('message'))
 <div class="col-md-12">
 <div class="alert alert-info">{{ Session::get('message') }}</div>
 </div>
@endif
  <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Article Title</th>
              <th scope="col">Article Content</th>
              <th scope="col">Author Email</th>
              <th scope="col"></th>
               <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @foreach($articles as $article)
            <tr>
                <td>{{ $article->id }}</td>
                <td>{{ $article->title }}</td>
                <td>{{ $article->content }}</td>
                <td>{{ $article->author_email }}</td>
                <td><a href="{{route('admin.edit-article',['id' => $article->id])}}" >Edit</a></td>
                <td><a href="{{route('admin.delete-article',['id' => $article->id])}}">Delete</a></td>
                @endforeach   
            </tr>
        </tbody>
    </table>
</div>
@endsection

