<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="/tasks">Blog Manager</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">

            <li class="nav-item">
                @if (Route::has('login'))
                @auth                       
            <li class="nav-item"> <a class="nav-link" href="{{ url('/') }}">Home</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('admin.create-article') }}"> Create Article </a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('admin.blog-dashboard') }}">Article Dashboard</a></li>
            @else                    
            <li class="nav-item"> <a class="nav-link" href="{{ route('login') }}">Login</a></li>
            <li class="nav-item"> <a class="nav-link"href="{{ route('register') }}">Register</a></li>

            @endauth

            @endif


        </ul>
        @if (Route::has('login'))
        @auth      
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a  href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li >
                        <a  href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
        @endauth

        @endif

    </div>
</nav>