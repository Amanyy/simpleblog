@extends('layouts.layout')
@section('content')
<div class="container">
    @if (Session::has('message'))
    <div class="col-md-12">
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    </div>
    @endif

    <h1> {{ $article->title }}</h1>
    <div class="jumbotron text-center">
        <p>
            <strong>
                {{ $article->content }}
            </strong><br>
        </p>
    </div>

    <h3>Comments</h3>
    {{ Form::open(['route' => ['store-comment'], 'method' => 'POST']) }}
    @if(!empty($errors->first('body')))
    <div class="alert alert-danger">{{ $errors->first('body') }}</div>
    @endif
    <p>{{ Form::textarea('body', old('body')) }}</p>
    {{ Form::hidden('article_id', $article->id) }}
    <p>{{ Form::submit('ADD') }}</p>
    {{ Form::close() }}
    @forelse ($article->comments as $comment)
    <p> {{$comment->created_at}}</p>
    <p>{{ $comment->body }}</p>
    <hr>
    @empty
    <p>This Article has no comments</p>
    @endforelse
</div>
@endsection


