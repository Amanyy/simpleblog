@extends('layouts.layout')
@section('content')
<div class="container">
    <div class="row">
        <div class="postAdd col-lg-12 text-right" style="margin-bottom: 20px;">
        </div>  
    </div>

    @if (Session::has('message'))
    <div class="col-md-12">
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    </div>
    @endif

    <div class="form-group">
        <br><br><br>
        {{ Form::label('Filter By Category', 'Filter By category',['class' => 'thead-dark'])}}{{ Form::open(array('id' => 'form-filter')) }}
        {{ Form::select('category', $categories, 'default', array('id' => 'categories')) }}
        {{ Form::close() }}
    </div>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Article Title</th>
                <th scope="col">Article Content</th>
                <th scope="col">category</th>

            </tr>
        </thead>
        <tbody>

            @foreach($articles as $article)
            <tr>
                <td>{{ $article->id }}</td>
                <td><a href="/show-article/{{$article->id}}">{{$article->title}}</a></td>
                <td>{{ $article->content }}</td>
                <td>{{ $article->category->name }}</td>

                @endforeach   
            </tr>
        </tbody>
    </table>
</div>
@endsection

<script>

    document.addEventListener("DOMContentLoaded", function (event) {
        $(function () {
            $('#form-filter').change(function () {
                var value = $('#categories').val();
                $("table tr").each(function (index) {
                    if (value == 0) {
                        $(this).show();
                    }
                    if (index != 0 && value != 0) {

                        $row = $(this);
                        var id = $row.find('td:eq(3)').text();

                        if (id.indexOf(value) != 0 && value != 0) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    }

                });
            });
        });
    });
</script>
