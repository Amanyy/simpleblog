<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', 'ArticleController@show')->name('home');
Route::get('/show-article/{id}', 'ArticleController@get_article')->name('show-article');
Route::post('/store-comment', 'CommentController@store_comment')->name('store-comment');

Auth::routes();
Route::group(['middleware' => ['auth'], 'prefix' => '/admin'], function () {
    //only authorized users can access these routes
    Route::group(array('namespace' => 'Admin'), function () {
        Route::get('/blog-dashboard', 'ArticleController@show')->name('admin.blog-dashboard');
        Route::get('/create-article', 'ArticleController@create_article')->name('admin.create-article');
        Route::post('/store-article', 'ArticleController@store_article')->name('admin.store-article');
        Route::get('/delete/{id}', 'ArticleController@delete_article')->name('admin.delete-article');
        Route::get('/edit/{id}', 'ArticleController@get_edit_article')->name('admin.edit-article');
        Route::post('/edit/{id}', 'ArticleController@post_edit_article')->name('admin.post-edit-article');
    });
});
Route::get('/tasks', 'ArticleController@show');
/*
 
 */