<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //
        DB::table('categories')->delete();
        $categories = [
            [1, 'Theft', '2019-07-12 22:00:00', NULL],
            [2, 'Magazine', '2019-07-12 22:00:00', NULL],
            [3, 'Community', '2019-07-12 22:00:00', NULL],
            [4, 'Squib', '2019-07-12 22:19:00', NULL]
        ];


        foreach ($categories as $category) {
            \App\Category::create(
                    [
                        'id' => $category[0],
                        'name' => $category[1],
                        'created_at' => $category[2],
                         'updated_at' => $category[3]
                    ]
            );
        }
    }

}
