
## Instalation guids 

* clone the project using git clone https://Amanyy@bitbucket.org/Amanyy/simpleblog.git
* Rename .enve.example into .env
* create DB nd name it blogging_system with username : root and no password 

``` 
run php artisan migrate

``` 
``` 
php artisan db:seed

```
```
 php artisan key:generate
 
```
```
 php artisan serve 
 
```



## Access Project 
- ** You can access login using email admin@admin.com and password admin or create a new account **

## Project structure sequence 

* i followed the repository design pattern in this project as it will make the code easier to manage and easier to understand with minimum response time  also
 Another benefit is how easy it makes it swap out the backend technology so i used  

- ** ArticleRepositoryInterface which acted like a contract for our repositorie **
- ** in ArticleRepository specify that we want to implement our ArticleRepositoryInterface **
- ** in BackendServiceProvider this file will serve as a connector to Laravelís IoC Container and allow me to use dependency injection to inject my repository interfaces**
- ** The same for CommentRepositoryInterface ,  CommentRepository **
* I have avoid lazy loading problem in the relation between article and category and so on 
* I have used a custom request files to mange the process of validate the data in a correct way in articles and comments

