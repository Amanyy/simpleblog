<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    //
    protected $table = 'comments';
    protected $fillable = ['id', 'article_id', 'body'];

    public function articles() {
        return $this->belongsTo(Article::class);
    }

}
