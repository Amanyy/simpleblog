<?php

namespace App\Http\Controllers;

use App\Repositories\ArticleRepositoryInterface;
use Session;
use App\Http\Requests\ArticleRequest;
use Illuminate\Support\Facades\Input;
use App\Category;

class ArticleController extends Controller {

    protected $articleRepo;

    /**
     * ArticleController constructor.
     *
     * @param ArticleController $articleRepo
     */
    public function __construct(ArticleRepositoryInterface $articleRepo) {
        $this->articleRepo = $articleRepo;
    }

    /**
     * List all articles.
     *
     * @return mixed
     */
    public function show() {
        //to avoide lazy loading problem
        $articles = $this->articleRepo->allWith('category');
        $categories = Category::orderBy('id', 'DESC')->get()->pluck('name', 'name');
        $categories->prepend('All');
        return view('article.article_list', compact('articles', 'categories'));
    }

    public function get_article($article_id) {
        $article = $this->articleRepo->get($article_id);
        return view('article.article', compact('article'));
    }

}
