<?php

namespace App\Http\Controllers;

use App\Repositories\CommentRepositoryInterface;
use App\Http\Controllers\Controller;
use Session;
use App\Http\Requests\CommentRequest;
use App\Category;
use Illuminate\Support\Facades\Redirect;

class CommentController extends Controller {

    protected $commentRepo;

    /**
     * CommentController constructor.
     *
     * @param CommentController $commentRepo
     */
    public function __construct(CommentRepositoryInterface $commentRepo) {
        $this->commentRepo = $commentRepo;
    }

    /**
     * List all articles.
     *
     * @return mixed
     */
    public function store_comment(CommentRequest $request) {
        $this->commentRepo->create($request->all());
        return Redirect::back()->with('message', 'Comment Added Sucessfully ');
    }

}
