<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\ArticleRepositoryInterface;
use App\Http\Controllers\Controller;
use Session;
use App\Http\Requests\ArticleRequest;
use App\Category;
class ArticleController extends Controller {

    protected $articleRepo;

    /**
     * ArticleController constructor.
     *
     * @param ArticleController $articleRepo
     */
    public function __construct(ArticleRepositoryInterface $articleRepo) {
        $this->articleRepo = $articleRepo;
    }

    /**
     * List all articles.
     *
     * @return mixed
     */
    public function show() {

        $articles = $this->articleRepo->allWith('category');
        return view('admin.article_dashboard', compact('articles'));
    }

    public function create_article() {
        $categories=Category::orderBy('id', 'DESC')->get()->pluck('name', 'id');
        return view('admin.add_article')->with('categories', $categories);
    }

    public function store_article(ArticleRequest $request) {
        $this->articleRepo->create($request->all());
        return redirect()->route('admin.blog-dashboard')->with('message', 'Article is added successfully');
    }

    public function delete_article($article_id) {
        $this->articleRepo->delete($article_id);
        return redirect()->route('admin.blog-dashboard')->with('message', 'Blog deleted successfully');
    }

    public function get_edit_article($article_id) {
        $article = $this->articleRepo->get($article_id);
        $categories=Category::orderBy('id', 'DESC')->get()->pluck('name', 'id');
        return view('admin.edit_article', compact('article','categories'));
    }

    public function post_edit_article($article_id, ArticleRequest $request) {

        $this->articleRepo->update($article_id, $request->all());
        Session::flash('message', 'Successfully updated your blog!');
        return redirect()->route('admin.blog-dashboard')->with('message', 'Successfully updated your blog');
    }

}
