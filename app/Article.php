<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

    //
    protected $table = 'articles';
    protected $fillable = ['category_id', 'title', 'content', 'author_email'];

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function comments() {
        return $this->hasMany(Comment::class);
    }

}
