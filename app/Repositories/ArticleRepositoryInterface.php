<?php

namespace App\Repositories;

interface ArticleRepositoryInterface {
 /**
     * Get's all article.
     *
     * @return mixed
     */
    public function get($article_id);
    /**
     * Create an article.
     *
     */
   
    public function create($article_data);

    /**
     * get all records based on with  to avoid lazy loading problem.
     *
     */
    public function allWith($with);

    /**
     * Deletes a article.
     *
     * @param int
     */
    public function delete($article_id);

    /**
     * Updates a article.
     *
     * @param int
     * @param array
     */
    public function update($article_id, array $article_data);
}
