<?php

namespace App\Repositories;

interface CommentRepositoryInterface {
        /**
     * create a comment.
     *
     * @param array
     */

    public function create($comment_data);

}
