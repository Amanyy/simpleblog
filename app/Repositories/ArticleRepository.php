<?php

namespace App\Repositories;

use App\Article;

class ArticleRepository implements ArticleRepositoryInterface {
    /**
     * Updates a article.
     *
     * @param array
     */

    /**
     * Get's a article by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($article_id) {
        return Article::find($article_id);
    }

    /**
     * Get's all articles.
     *
     * @return mixed
     */
    public function allWith($with) {
        return Article::with($with)->get();
    }

    /**
     * Deletes a article.
     *
     * @param int
     */
    public function delete($article_id) {
        Article::destroy($article_id);
    }

    /**
     * Updates a article.
     *
     * @param int
     * @param array
     */
    public function update($article_id, array $article_data) {
        Article::find($article_id)->update($article_data);
    }
     /**
     * Create a article.
     *
     * @param arr
     */

    public function create($article_data) {
        return Article::create($article_data);
    }

}
