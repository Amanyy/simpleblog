<?php

namespace App\Repositories;

use App\Comment;

class CommentRepository implements CommentRepositoryInterface {
    /**
     * create a comment.
     *
     * @param array
     */
    public function create($comment_data) {
        return Comment::create($comment_data);
    }

}
